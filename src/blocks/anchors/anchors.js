$(document).ready(function () {
  var scrollOffset = 150;
  
  $('body').scrollspy({
    target: '#scroll-navbar',
    offset: scrollOffset
  });
  $(window).on('activate.bs.scrollspy', function (e, obj) {
    var links = $('.js-link-menu');
    links.removeClass('is-active')
      .filter('[href="' + obj.relatedTarget + '"]').addClass('is-active');
    $('.menu-popup__img').removeClass('is-active');
    links.each(function (i, el) {
      if ($(el).hasClass('is-active')) {
        $('.menu-popup__img')[i].classList.add('is-active');
      }
    })
  });
});
