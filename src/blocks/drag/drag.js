$(document).ready(function(){
  $('.drag__wrapper').each(function(i,el){
    var img = $(el).find('.drag__img');
    var drag = $(el).find('.drag__content');
    drag.dragscroll();
    $(el).scrollLeft(500)
      .scrollTop(500);
  });
});

var planSize = 100;
var plan = $('.drag__img');

$('.drag__btn--minus').click(function(e) {
  e.preventDefault();
  if (planSize <= 100) {
    planSize = 100;
  }
  else {
    planSize -= 50;
  }
  plan.css('max-width', planSize + '%').css('max-height', planSize + '%');
})

$('.drag__btn--plus').click(function(e) {
  e.preventDefault();
  if (planSize >= 500) {
    planSize = 500;
  }
  else {
    planSize += 50;
  }
  plan.css('max-width', planSize + '%').css('max-height', planSize + '%');
})
