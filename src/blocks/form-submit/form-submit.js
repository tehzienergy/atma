$(document).ready(function () {
  $('.js-submit').submit(function (e) {
    e.preventDefault();
    var form = $(this);
    var form_data = form.serialize();
    form.trigger('reset');
    $.ajax({
      type: 'POST',
      url: './send.php',
      data: form_data,
      success: function (data) {
        $.fancybox.close();
        $.fancybox.open({
          src: '#modal-done',
          touch: 'false',
          baseClass: 'fancybox--full'
        });
      }
    });
  });
  $('.js-masked-tel').mask("+79999999999");
});

$(document).ready(function () {
  $('.js-subs-submit').submit(function (e) {
    e.preventDefault();
    var form = $(this);
    var form_data = form.serialize();
    form.trigger('reset');
    $.ajax({
      type: 'POST',
      url: './subscribe.php',
      data: form_data,
      success: function (data) {
        $.fancybox.close();
        $.fancybox.open({
          src: '#modal-subs-done',
          touch: 'false',
          baseClass: 'fancybox--full'
        });
      }
    });
  });
});

$('input.form-control').change(function() {
  $(this).addClass('input-active');
  if( !$(this).val() ) {
    $(this).removeClass('input-active');
  }
});
