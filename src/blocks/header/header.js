$(window).scroll(function () {
  var w = $(window);
  if (w.width() > 767 && w.scrollTop() > w.height()) {
    $('#header').addClass('is-sticky');
    $('.js-fix-link-right').fadeIn('fast');
  } else {
    $('#header').removeClass('is-sticky');
    $('.js-fix-link-right').fadeOut('fast');
  }
});
