$(document).ready(function(){
  var menuImgs = $('.menu-popup__img');
  $('.js-link-menu').each(function(i,el){
    $(el).click(function(){
      $.fancybox.close(true);
      setTimeout(function(){
        menuImgs.removeClass('is-active');
        menuImgs[0].classList.add('is-active');
      },500);
    }).hover(function(){
      menuImgs.removeClass('is-active');
      menuImgs[i].classList.add('is-active');
    });
  });
});
