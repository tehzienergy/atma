$(document).ready(function () {
  if ($(window).width() > 991) {
    var bgOffset = -120;
    
    

    if ($(window).height() < 950) {
      bgOffset = 0;
    }
    
    if ($(window).width()/$(window).height() < 1.65) {
      bgOffset = 0;
    }

    $('.js-parallax-index').parally({
      speed: -0.3,
      offset: bgOffset
    });

    $('.js-parallax').parally({
      speed: -0.2,
      mode: 'transform',
      offset: 100 * 0.2
    });
  }

});
