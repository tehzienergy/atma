$(document).ready(function(){
  ScrollReveal().reveal('.js-reveal-interval',{
    distance: '100px',
    interval: 600
  });
  ScrollReveal().reveal('.js-reveal',{
    distance: '50px',
    delay: 50,
    duration: 600
  });
});
