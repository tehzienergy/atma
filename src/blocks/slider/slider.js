jQuery(document).ready(function ($) {

  $('.slider__content').slick({
    infinite: false,
    variableWidth: true,
    arrows: false,
    dots: false,
    swipeToSlide: true,
    focusOnSelect: true,
    responsive: [
      {
        breakpoint: 992,
        settings: {
          variableWidth: false,
          dots: true
        }
      }
    ]
  })

})
