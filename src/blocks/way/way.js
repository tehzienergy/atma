// Как добраться

function initWay(){
  if($(".routeSlide").length>0){
    
//Кнопка очистить. Отображение
    function checkSearchRouteItput(){
      var input=$("#searchRouteItput");
      var inputVal=input.val();
      if (inputVal!=""){
        $(".way__controls").removeClass("is-clear");
      }else{
        $(".way__controls").addClass("is-clear");
      }
    }
    checkSearchRouteItput();

    // $("#searchRouteItput").change(function(){
    //   checkSearchRouteItput();
    // });
    // $("#searchRouteItput").keypress(function(){
    //   checkSearchRouteItput();
    // });
    // $("#searchRouteItput").keyup(function(){
    //   checkSearchRouteItput();
    // });
    // $('#searchRouteItput').on('paste', function () {
    //   var element = this;
    //   setTimeout(function () {
    //     checkSearchRouteItput();
    //   }, 100);
    // });

//Быстрый ввод ссылок
    $(".way__link").click(function(){
      $("#searchRouteItput").val($(this).text());
      //checkSearchRouteItput();
      return false;
    });

// Карта
// Сбор данных карты
    
    var villageLongitude=Number($(".routeSlide").data("longitude"));
    var villageLatitude=Number($(".routeSlide").data("latitude"));

    var routeMap;
    var URLMarkerImg="./img/map-marker-color.svg";
    var routePointA=[villageLongitude, villageLatitude]; //точка A
    var routePointB=[55.832611, 37.395317]; //точка B
    var routeOnMap;

//Посроить маршрут
    function makeRout(){
      routePointB=$("#searchRouteItput").val();
      var boundsAutoApply=false;
      //routePointB="Белгород";
      if(routePointB.trim()!=""){
        boundsAutoApply=true;
      }

      routeMap.geoObjects.removeAll();
      routeMap.setCenter(routePointA, 9);
      routeMap.container.fitToViewport();

      balloonLayout = ymaps.templateLayoutFactory.createClass(
        "<div class='route-balloon'><div class='inner-balloon'>" +

        "<span class='title'>На автомобиле</span>" +
        "<div class='line'></div>" +
        "<span>~{{ properties.duration.text }}</span>" +
        "</div><div class='triugol-balloon'></div>", {

          build: function () {
            this.constructor.superclass.build.call(this);
            this._$element = $('.my-balloon', this.getParentElement());
            this._$element.find('.close')
              .on('click', $.proxy(this.onCloseClick, this));
          }
        }
      );

      routeOnMap = new ymaps.multiRouter.MultiRoute(
        {
          referencePoints: [routePointB,routePointA]},
        {
          boundsAutoApply: boundsAutoApply,

          // Внешний вид линии маршрута.
          routeStrokeWidth: 4,
          routeStrokeOpacity: 1,
          routeActiveStrokeWidth: 4,
          routeActiveStrokeOpacity: 1,
          routeActiveStrokeColor: "#000",

          // Внешний вид путевых точек.
          wayPointStartIconLayout: "default#image",
          wayPointStartIconImageHref: "./img/marker-b.png",
          wayPointStartIconImageSize: [82, 87],
          wayPointStartIconImageOffset: [-41, -82],
          wayPointFinishIconLayout: "default#image",
          wayPointFinishIconImageHref: "./img/marker-a.png",
          wayPointFinishIconImageSize: [82, 87],
          wayPointFinishIconImageOffset: [-41, -82],
          pinVisible:false,
          
          zoomMargin: 30,

          balloonLayout: balloonLayout,
          // Отключаем режим панели для балуна.
          balloonPanelMaxMapArea: 0

        });

      if(routePointB.trim()!=""){
        // Подписка на событие обновления данных маршрута.
        routeOnMap.model.events.add('requestsuccess', function() {
          var activeRoute = routeOnMap.getActiveRoute();

          activeRoute.balloon.open();
          // console.log("Длина: " + activeRoute.properties.get("distance").text);
          // console.log("Время прохождения: " + activeRoute.properties.get("duration").text);

          $("#timeRouteBoxDesc").html("Дорога на автомобиле от: "+routePointB+" за "+activeRoute.properties.get("duration").text);
          $("#routeMotionTime").html("Время в пути: ~"+activeRoute.properties.get("duration").text);

          $("#timeRouteBox").addClass("show");
          $("#routeMotionTime").addClass("show");
          $("#routeDescriptionBox").addClass("show");
          $("#routeSampleB").removeClass("show");

        });
      }else{
        $("#timeRouteBox").removeClass("show");
        $("#routeMotionTime").removeClass("show");
        $("#routeDescriptionBox").removeClass("show");
        $("#routeSampleB").addClass("show");
      }

      routeMap.geoObjects.add(routeOnMap);
      checkSearchRouteItput();
    }

// Инициализируем яндек карту
    function initRouteMap(){
      // Создание карты.    
      routeMap = new ymaps.Map("routeMap", {
        // Координаты центра карты.
        // Порядок по умолчанию: «широта, долгота».
        // Чтобы не определять координаты центра карты вручную,
        // воспользуйтесь инструментом Определение координат.
        center: routePointA,
        // Уровень масштабирования. Допустимые значения:
        // от 0 (весь мир) до 19.
        zoom: 9,
        controls: [],  // 'zoomControl' , 'routeButtonControl'
        duration: 300,
      });

      routeMap.behaviors.disable('scrollZoom');

      if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|BB|PlayBook|IEMobile|Windows Phone|Kindle|Silk|Opera Mini/i.test(navigator.userAgent)) {
        routeMap.behaviors.disable('drag');
      }

      var zoomControl = new ymaps.control.ZoomControl({
        options: {
          size: "small",
          position: {
            top: '30px',
            left: '30px'
          }
        }
      });
      routeMap.controls.add(zoomControl);
      routeMap.controls.remove('gotoymaps');

      makeRout();


    }
    
    initRouteMap();
    
// Построение маршрута при клике
    $("#searchRouteForm").submit(function(){
      makeRout();
      return false;
    });

// Очистка маршрута
    $("#clearRoutButton").click(function(){
      $("#searchRouteItput").val("");
      $("#searchRouteItput").focus();

      // Отображение / Скрытие блоков
      $("#timeRouteBox").removeClass("show");
      $("#routeMotionTime").removeClass("show");
      $("#routeDescriptionBox").removeClass("show");
      $("#routeSampleB").addClass("show");

      makeRout();
      //checkSearchRouteItput();
      return false;
    });

  }
  return true;
}
